// imports the User model
const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Controller function for checking email duplicates
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}
		else{
			return false;
		}
	})
}

// Controller function for user registration
//Creates variable "newUser" and instantiates a new "User" object using the mongoose model
module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email: reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	// Saves the created object to our database
	return newUser.save().then((user, error) => {
		// User registration failed
		if(error){
			console.log(error);
			return false;
		}
		// User registration successful
		else{
			return true;
		}
	})
}

//User authetication (/login)
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result=>{
		//User does not exist
		if(result == null){
			return false;
		}
		//User exists
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				//Generate an access token 
				return{access : auth.createAccessToken(result)}
			}
			//Passwords do not match
			else{
				return false;
			}
		}
	})
}

// ACTIVITY

//Getting all the users
module.exports.getAllUsers = () => {
	return User.find({}).then(result => {
		return result;
	})
}

// Retrieving the details of the user
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then((specificUser, err) => {
		if(err){
			console.log(err);
			return false;
		}
			
		else{
			specificUser.password = ""
			return specificUser;

		}
	})
}

// Delete a user
/*module.exports.deleteUser= (userId) => {
	return User.findByIdAndDelete(userId).then((removedUser, err) => {
		if(err){
			console.log(err);
			return false;
		}
		else{
			removedUser.password = ""
			return removedUser;
		}
	})
}*/

module.exports.enroll = async (data) => {
	// Add the course Id in the enrollments array of the user
	let isUserUpdated = await User.findById(data.userId).then(user => {
		// Adds the courseId in the user's enrollments array
		user.enrollments.push({courseId : data.courseId});

		// Save the updated user information
		return user.save().then((user, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})
	// Add the user Id in the enrollees array of the course
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		// Adds the userId in the course's enrollees array
		course.enrollees.push({userId : data.userId});

		// Saves the updated course information in the database
		return course.save().then((course, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})

	// Condition that will if the user and course documents have been updated
	// User enrollment is successful
	if(isUserUpdated && isCourseUpdated){
		return true;
	}
	// User enrollment failure
	else{
		return false;
	}
}





